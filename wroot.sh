#!/bin/bash

# только CentOS

# cat php-fpm.pool | sed 's/{$user}/user_ru/' > $WWW/$user/.php-fpm.on
# cat nginx.pool | sed 's/{$user}/vexe4_ru/' | sed 's/{$url}/vexe4.ru/' > /www/vexe4_ru/.nginx.on
# /etc/sysconfig/php-fpm >> umask 067

VERSION=1.0

# Начало работы:
# 1. копируем папку wroot, в потайное место установки, идеальное место /root/*
# 2. заполняем данные подключения БД и каталогов сайтов, каталог (указать WWW, будет создан при отсутствие)
# 3. запускаем скрипт на установку /root/wroot/wroot.sh install
# 4. 
# 5. 

# скрытый каталог админа, обычно /root
WROOT=`dirname $(readlink -e $0)`

# каталог сайтов, обычно /www
WWW=/www

# проверка прав доступа
sudo true || exit 1

# данные для подключения к бд
DB_NAME=wroot
DB_USER=root
DB_PASW=`cat $WROOT/.key`

# проверка наличия пакетов (+извлечение их полных путей)
z=`type whereis`
if [ -z "$z" ]; then
	echo '"whereis" not found.'
	exit 1
fi
pwgen=`whereis pwgen | awk '{print $2}'`
if [ -z "$pwgen" ]; then
	echo '"pwgen" not found.'
	exit 1
fi
mysql=`whereis mysql | awk '{print $2}'`
if [ -z "$mysql" ]; then
	echo '"mysql" not found.'
	exit 1
fi
nginx=`whereis nginx | awk '{print $2}'`
if [ -z "$nginx" ]; then
	echo '"nginx" not found.'
	exit 1
fi
php=`whereis php | awk '{print $2}'`
if [ -z "$php" ]; then
	echo '"php" not found.'
	exit 1
fi
phpfpm=`whereis php-fpm | awk '{print $2}'`
if [ -z "$phpfpm" ]; then
	phpfpm=`whereis php5-fpm | awk '{print $2}'`
	if [ -z "$phpfpm" ]; then
		echo '"php" not found.'
		exit 1
	fi
fi

# запрос в mysql 
#  $1   - запрос
#  [$2] - стиль вывода (def:HIDE)
function sql()
{
	if [ $# -lt 1 ]; then
		return 1
	fi

	local p
	if [ -n "$DB_PASW" ]; then 
		p="-p$DB_PASW"
	fi

	case $2 in
		VERT)
			$mysql -u$DB_USER $p -D $DB_NAME -e "$1 \G" && return 0 || return 1
		;;
		LIST)
			$mysql -u$DB_USER $p -D $DB_NAME -e "$1" && return 0 || return 1
		;;
		NODB)
			$mysql -u$DB_USER $p -e "$1" 2> /dev/null 1> /dev/null && return 0 || return 1
		;;
		*)
			$mysql -u$DB_USER $p -D $DB_NAME -e "$1" 2> /dev/null 1> /dev/null && return 0 || return 1
		;;
	esac
}

case "$1" in
	-v|ver|version)
		echo "Wroot version $VERSION"
	;;

	testsql)
		DB_NAME="mysql"
		sql "SELECT NOW()" LIST
	;;

	# начальная установка и конфигурация
	install)
		if [ -d $WROOT/wroot-config ]; then
			echo "Already installed."
			exit 1
		fi

		# добавление группы
		groupadd wroot || exit 1

		# создаем корневой каталог и присваиваем права
		mkdir -m 2110 $WWW 2> /dev/null || chmod 2110 $WWW 2> /dev/null
		#chown -R wroot:wroot $WWW/*

		# добавление основного пользователя
		useradd -b $WWW -mk $WROOT/skel -s /bin/nologin -g wroot wroot 2> /dev/null || exit 1
		rm $WWW/wroot/.*.on

		# генерация пароля пользователю
		pasw=`$pwgen -s 10 1`
		usermod -p `openssl passwd -1 "$pasw"` wroot

		# создание системной и общей БД MySQL
		sql "CREATE DATABASE IF NOT EXISTS ${DB_NAME} COLLATE 'utf8_general_ci' " NODB
		sql "CREATE DATABASE IF NOT EXISTS ${DB_NAME}_common COLLATE 'utf8_general_ci' " NODB

		# таблица паролей
		sql "CREATE TABLE IF NOT EXISTS ${DB_NAME}.system ( \
			id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, \
			uid INT(11) UNSIGNED NOT NULL DEFAULT '0', \
			user VARCHAR(30) NOT NULL DEFAULT '', \
			original VARCHAR(30) NOT NULL DEFAULT '', \
			password VARCHAR(20) NOT NULL DEFAULT '', \
			ftp VARCHAR(20) NOT NULL DEFAULT '', \
			mysql VARCHAR(20) NOT NULL DEFAULT '', \
			enable TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
			added DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', \
			edited TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, \
			deadline DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00', \
			PRIMARY KEY (id) \
		) COLLATE='utf8_general_ci' ENGINE=MyISAM;" NODB

		# таблица сервиса Pure-FTPd
		sql "CREATE TABLE IF NOT EXISTS ${DB_NAME}.ftp ( \
			User VARCHAR(16) BINARY NOT NULL, \
			Password VARCHAR(64) BINARY NOT NULL, \
			Uid INT(11) NOT NULL default '-1', \
			Gid INT(11) NOT NULL default '-1', \
			Dir VARCHAR(128) BINARY NOT NULL, \
			PRIMARY KEY  (User) \
		) COLLATE='utf8_general_ci' ENGINE=MyISAM;" NODB

		# создание пользователя БД, для сервиса Pure-FTPd
		pftpd=`$pwgen -s 10 1`
		sql "CREATE USER 'ftp'@'localhost' IDENTIFIED BY '$pftpd' " NODB
		sql "GRANT USAGE ON *.* TO 'ftp'@'localhost' " NODB
		sql "GRANT SELECT ON ${DB_NAME}.ftp TO 'ftp'@'localhost' " NODB
		sql "FLUSH PRIVILEGES " NODB

		# создание пользователя БД, для общей таблицы
		pmysql=`$pwgen -s 10 1`
		sql "CREATE USER 'wroot'@'localhost' IDENTIFIED BY '$pmysql' " NODB
		sql "GRANT USAGE ON *.* TO 'wroot'@'localhost' " NODB
		sql "GRANT SELECT, EXECUTE, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, \
			CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES \
			ON ${DB_NAME}_common.* TO 'wroot'@'localhost' WITH GRANT OPTION " NODB
		sql "FLUSH PRIVILEGES " NODB

		# нахождение uid
		uid=`id wroot | grep -oP "uid=([0-9]+)" | grep -oP "[0-9]+"`
		gid=`id wroot | grep -oP "gid=([0-9]+)" | grep -oP "[0-9]+"`

		# сохранение данных wroot & ftp & mysql в MySQL
		pftp=`$pwgen -s 10 1`
		sql "INSERT INTO ${DB_NAME}.system (uid,user,original,password,mysql,ftp,enable,added) \
			VALUES ('$uid','wroot','wroot','$pasw','$pmysql','$pftp',1,NOW()) "
		sql "INSERT INTO ${DB_NAME}.ftp (Uid,Gid,Dir,User,Password) \
			VALUES ('$uid','$gid','$WWW/wroot/template','wroot',MD5('$pftp')) " 

		# создание ссылки и установка прав на запуск root
		self=`pwd`/`basename $0`
		ln -s $self /usr/local/bin/wroot 2> /dev/null
		#chmod 500 /usr/local/bin/wroot

		# установка прав на каталог
		chown -R wroot:wroot $WWW 2> /dev/null
		chmod -R 2710 $WWW/* 2> /dev/null
		# unmask file:dir 750:750 # todo: !!!

		echo "Installation success,"
		echo " linked: $self -> /usr/local/bin/wroot"
		echo ""
		echo "Administrator, "
		echo " system-user:wroot password:$pasw"
		echo " FTP:   user:wroot password:$pftp"
		echo " MySQL: user:wroot password:$pmysql"
		echo ""
		echo " Pure-FTPd, mysql-connection:  db:$DB_NAME table:ftp user:ftp password:$pftpd"
		echo ""
		echo " Generated configs, please check it in \"$WROOT/wroot-config\""
		echo ""

		mkdir $WROOT/wroot-config
		# todo: генерировать конфиги pure-ftpd & mysql & php & nginx & php-fpm
#pure-ftpd.conf2 && sed '/^\s*NoAnonymous/s/\S*\s*$/yes/' pure-ftpd.conf > pure-ftpd.conf2 && diff pure-ftpd.conf pure-ftpd.conf2
	;;

	""|list)
		sql "SELECT id,user,original,enable,added,\
			TRUNCATE((UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(added))/86400,0) AS elapsed, \
			TRUNCATE((IF(UNIX_TIMESTAMP(deadline)>0,UNIX_TIMESTAMP(deadline),UNIX_TIMESTAMP(NOW()))-UNIX_TIMESTAMP(NOW()))/86400,0) AS remain
			FROM system ORDER BY id" LIST

		echo ""
		echo "  use: wroot <site> on | off | add | add_empty | del | ftp | mysql"
		echo ""
	;;

	*)
		# извлекаем возможный домен
		user_origin=$1
		if [ "$user_origin" != "wroot" ]; then
			user_origin=$(echo "$1" | grep -oP [-_a-z0-9\\.]+[_\\.][a-z]+)
		fi
		user=${user_origin//./_}

		if [ -z "$user_origin" ]; then
			echo "please select user"
		else
			case "$2" in
				add)
					# существует ли пользователь
					if [ -d $WWW/$user ]; then
						echo "User '$WWW/$user' already exists."
						exit 1
					fi

					# добавление пользователя
					useradd -b $WWW -mk $WROOT/skel -s /bin/bash -g wroot $user 2> /dev/null || exit 1

					# генерация пароля пользователю
					pasw=`$pwgen -s 10 1`
					usermod -p `openssl passwd -1 "$pasw"` $user

					chmod 2710 $WWW/$user

					# создание БД MySQL
					sql "CREATE DATABASE IF NOT EXISTS $user COLLATE 'utf8_general_ci' " NODB
					# и пользователя для доступа к ней
					pmysql=`$pwgen -s 10 1`
					sql "CREATE USER '$user'@'localhost' IDENTIFIED BY '$pmysql' " NODB
					sql "GRANT USAGE ON *.* TO '$user'@'localhost' " NODB
					sql "GRANT SELECT ON ${DB_NAME}_common.* TO '$user'@'localhost' " NODB
					sql "GRANT SELECT, EXECUTE, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, \
						CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES \
						ON $user.* TO '$user'@'localhost' WITH GRANT OPTION " NODB
					sql "FLUSH PRIVILEGES " NODB

					# нахождение uid
					uid=`id $user | grep -oP "uid=([0-9]+)" | grep -oP "[0-9]+"`
					gid=`id wroot | grep -oP "gid=([0-9]+)" | grep -oP "[0-9]+"`

					# сохранение данных wroot & ftp & mysql в MySQL
					pftp=`$pwgen -s 10 1`
					sql "INSERT INTO ${DB_NAME}.system (uid,user,original,password,mysql,ftp,enable,added) \
					  VALUES ('$uid','$user','$user_origin','$pasw','$pmysql','$pftp',1,NOW()) "
					sql "INSERT INTO ${DB_NAME}.ftp (Uid,Gid,Dir,User,Password) \
					  VALUES ('$uid','$gid','$WWW/$user/template','$user',MD5('$pftp')) " 

					echo "User added,"
					echo " system-user:$user password:$pasw"
					echo " FTP:   user:$user password:$pftp"
					echo " MySQL: user:$user password:$pmysql"
					echo ""
				;;

				add_empty)
					# существует ли пользователь
					if [ -d $WWW/$user ]; then
						echo "User '$WWW/$user' already exists."
						exit 1
					fi

					# добавление пользователя
					useradd -b $WWW -mk $WROOT/empty -s /bin/bash -g wroot $user 2> /dev/null || exit 1

					# генерация пароля пользователю
					pasw=`$pwgen -s 10 1`
					usermod -p `openssl passwd -1 "$pasw"` $user

					chmod 710 $WWW/$user

					# создание БД MySQL
					sql "CREATE DATABASE IF NOT EXISTS $user COLLATE 'utf8_general_ci' " NODB
					# и пользователя для доступа к ней
					pmysql=`$pwgen -s 10 1`
					sql "CREATE USER '$user'@'localhost' IDENTIFIED BY '$pmysql' " NODB
					sql "GRANT USAGE ON *.* TO '$user'@'localhost' " NODB
					sql "GRANT SELECT, EXECUTE, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, \
						CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES \
						ON $user.* TO '$user'@'localhost' WITH GRANT OPTION " NODB
					sql "FLUSH PRIVILEGES " NODB

					# нахождение uid
					uid=`id $user | grep -oP "uid=([0-9]+)" | grep -oP "[0-9]+"`
					gid=`id wroot | grep -oP "gid=([0-9]+)" | grep -oP "[0-9]+"`

					# сохранение данных wroot & ftp & mysql в MySQL
					pftp=`$pwgen -s 10 1`
					sql "INSERT INTO ${DB_NAME}.system (uid,user,original,password,mysql,ftp,enable,added) \
					    VALUES ('$uid','$user','$user_origin','$pasw','$pmysql','$pftp',1,NOW()) "
					sql "INSERT INTO ${DB_NAME}.ftp (Uid,Gid,Dir,User,Password) \
					    VALUES ('$uid','$gid','$WWW/$user/template','$user',MD5('$pftp')) " 

					echo "User added,"
					echo " system-user:$user password:$pasw"
					echo " FTP:   user:$user password:$pftp"
					echo " MySQL: user:$user password:$pmysql"
					echo ""	
				;;

				del)
					if [ "$user" == "wroot" ]; then
						echo "Can't delete common user."
						exit 1
					fi

					# отключение доступа HTTP
					echo -n "Stopping host - '$user' ..."
					mv $WWW/$user/.nginx.on $WWW/$user/.nginx.off 2> /dev/null || mv $WWW/$user/.nginx $WWW/$user/.nginx.off 2> /dev/null
					$nginx -s reload
					echo "done"

					# нахождение uid
					uid=`id $user | grep -oP "uid=([0-9]+)" | grep -oP "[0-9]+"`

					# удаление system пользователя
					echo -n "Delete system user '$user' ..."
					sql "DELETE FROM ${DB_NAME}.system WHERE user LIKE \"$user\" "
					sql "DELETE FROM ${DB_NAME}.ftp WHERE uid=\"$uid\" "
					userdel $user
					echo "done"

					# удаление пользователя БД
					sql "DROP USER '$user'@'localhost' "

					# физическое удаление
					echo "Delete files and drop DB ?"
					select s in 'no' 'yes' ; 
					do
						if [ "$s" = "yes" ]; then
							echo -n "Droping DB '$user' ..."
							sql "DROP DATABASE $user"
							echo "done"

							echo -n "Removing files from '$WWW/$user' ..."
							rm -rf $WWW/$user
							echo "done"

							break;
						fi

						if [ "$s" = "no" ]; then
							break;
						fi
					done
				;;

				on)
					echo -n "Starting host - '$user' ..."
					mv $WWW/$user/.nginx.off $WWW/$user/.nginx.on 2> /dev/null || mv $WWW/$user/.nginx $WWW/$user/.nginx.on 2> /dev/null
					$nginx -s reload
					sql "UPDATE ${DB_NAME}.system SET enable=1 WHERE user LIKE \"$user\" "
					echo "done"
				;;

				off)
					echo -n "Stopping host - '$user' ..."
					mv $WWW/$user/.nginx.on $WWW/$user/.nginx.off 2> /dev/null || mv $WWW/$user/.nginx $WWW/$user/.nginx.off 2> /dev/null
					$nginx -s reload
					sql "UPDATE ${DB_NAME}.system SET enable=0 WHERE user LIKE \"$user\" "
					echo "done"
				;;
				
				password|passwd|paswd|pasw)					
					# генерация пароля пользователю
					pasw=$3
					if [ "$pasw" = "rand" ]; then
						pasw=`$pwgen -s 10 1`
					fi

					usermod -p `openssl passwd -1 "$pasw"` $user
					
					sql "UPDATE ${DB_NAME}.system SET password=\"$pasw\" WHERE user LIKE \"$user\" "

					echo "new password: $pasw"
					echo ""
					
				;;

				ftp)
					# нахождение uid
					uid=`id $user | grep -oP "uid=([0-9]+)" | grep -oP "[0-9]+"`

					case "$3" in
						password|passwd|paswd|pasw)
							# генерация пароля пользователю
							pftp=$4
							if [ "$pftp" = "rand" ]; then
								pftp=`$pwgen -s 10 1`
							fi

							sql "UPDATE ${DB_NAME}.system SET ftp='$pftp' WHERE user LIKE \"$user\" "
							sql "UPDATE ${DB_NAME}.ftp SET Password=MD5('$pftp') WHERE Uid=$uid "

							echo "new password: $pftp"
							echo ""
						;;

						path)
							path=$4
							if [ -z $path ]; then
								exit 1
							fi

							path=$WWW/$user/$path
							path=${path//\/\//\/}

							if [ -d $path ]; then
								echo -n ""
							else
								echo "Directory not found '$path' "
								exit 1
							fi

							sql "UPDATE ${DB_NAME}.ftp SET Dir=\"$path\" WHERE Uid=$uid "
							echo "new path: $path"
							echo ""
						;;

						list|*)
							sql "SELECT user,ftp,(SELECT Dir FROM ${DB_NAME}.ftp WHERE Uid=$uid) AS path \
								FROM ${DB_NAME}.system WHERE user LIKE \"$user\" " LIST

							echo ""
							echo "  use: path <new path> | paswd <new password|rand>"
							echo ""
						;;
					esac
				;;

				mysql)
					case "$3" in
						password|passwd|paswd|pasw)
							# генерация пароля пользователю
							pmysql=$4
							if [ "$pmysql" = "rand" ]; then
								pmysql=`$pwgen -s 10 1`
							fi

							sql "UPDATE ${DB_NAME}.system SET mysql='$pmysql' WHERE user LIKE \"$user\" "
							sql "SET PASSWORD FOR '$user'@'localhost' = PASSWORD('$pmysql') " NODB
							sql "FLUSH PRIVILEGES " NODB

							echo "new password: $pmysql"
							echo ""
						;;

						#add) # ждет версии 2, множество бд на одного пользователя, как и множетсво фтп
						#;;
						#del)
						#;;

						list|*)
							sql "SELECT user,mysql FROM ${DB_NAME}.system WHERE user LIKE \"$user\" " LIST

							echo ""
							echo "  use: paswd <new password|rand>"
							echo ""
							#echo "  use: add <new db> | del <db> | paswd <new password> [db]"
						;;
					esac
				;;

				*)
					# нахождение uid
					uid=`id $user | grep -oP "uid=([0-9]+)" | grep -oP "[0-9]+"`

					sql "SELECT *,\
						TRUNCATE((UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(added))/86400,0) AS elapsed, \
						TRUNCATE((IF(UNIX_TIMESTAMP(deadline)>0,UNIX_TIMESTAMP(deadline),UNIX_TIMESTAMP(NOW()))-UNIX_TIMESTAMP(NOW()))/86400,0) AS remain, \
						(SELECT Dir FROM ${DB_NAME}.ftp WHERE Uid=$uid) AS ftp_path
						FROM ${DB_NAME}.system WHERE user LIKE \"$user\" " VERT
				
					echo ""
					echo "  use: on | off | del | paswd <new password|rand> | ftp | mysql"
					echo ""
				;;
			esac
		fi
	;;
esac

